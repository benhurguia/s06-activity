-- PROVIDE THE ANSWER TO THE FOLLOWING:
-- a. List the books Authored by Marjorie Green.
-- The Busy Executive's Database Guide
-- You Can Combat Computer Stress!

-- b. List the books Authored by Michael O'Leary.
-- Cooking with Computers

-- c. Write the author/s of "The Busy Executives Database Guide".
-- Marjorie Green
-- Bennet Abraham

-- d. Identify the publisher of "But Is It User Friendly?".
-- Algodata InfoSystems

-- e. List the books published by Algodata Infosystems.
-- The Busy Executive's Database Guide
-- Cooking with Computers
-- Straight Talk About Computers
-- But is It User Friendly?
-- Secrets of Silicon Valley
-- Net Etiquette
    
-- CREATE SQL SYNTAX AND QUERIES TO CREATE A DATABASE BASED ON THE ERD:

--     1. Create a database for blog:
--         STATEMENT:
--             CREATE DATABASE blog_db;

--     2. Create a table for users:
--         STATEMENT:
--             CREATE TABLE users(
--                 id INT NOT NULL AUTO_INCREMENT,
--                 email VARCHAR(100) NOT NULL,
--                 password VARCHAR(300) NOT NULL,
--                 datetime_created DATETIME NOT NULL,
--                 PRIMARY KEY (id)
--             );

--     3. Create a table for table posts:
--         STATEMENT:
--             CREATE TABLE posts (
--                 id INT NOT NULL AUTO_INCREMENT,
--                 user_id INT NOT NULL,
--                 title VARCHAR(500) NOT NULL,
--                 content VARCHAR(5000) NOT NULL,
--                 datetime_posted DATETIME NOT NULL,
--                 PRIMARY KEY (id),
--                 CONSTRAINT fk_post_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
--             );

--     4. Create a table for table post_likes:
--         STATEMENT:
--             CREATE TABLE post_likes (
--                 id INT NOT NULL AUTO_INCREMENT,
--                 post_id INT NOT NULL,
--                 user_id INT NOT NULL,
--                 datetime_liked DATETIME NOT NULL,
--                 PRIMARY KEY (id),
--                 CONSTRAINT fk_post_like_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT,
--                 CONSTRAINT fk_post_like_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT
--             );

--     5. Create a table for table post_comments:
--         STATEMENT:
--             CREATE TABLE post_comments (
--                 id INT NOT NULL AUTO_INCREMENT,
--                 post_id INT NOT NULL,
--                 user_id INT NOT NULL,
--                 content VARCHAR(5000) NOT NULL,
--                 datetime_commented DATETIME NOT NULL,
--                 PRIMARY KEY (id),
--                 CONSTRAINT fk_post_comment_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
--                 CONSTRAINT fk_post_comment_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT
--             );
